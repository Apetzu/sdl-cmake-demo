#include <SDL.h>
#include <stdio.h>

int main(int argc, char* argv[])
{
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        printf("Could not initialize SDL. SDL_Error: %s\n", SDL_GetError());
    }
    else
    {
        SDL_CreateWindow(
            "SDL2 Demo",
            SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
            800, 600,
            SDL_WINDOW_SHOWN
        );
        
        SDL_Delay(2000);

        SDL_version compiled, linked;
        SDL_VERSION(&compiled);
        SDL_GetVersion(&linked);
        printf("Compiled against SDL version %d.%d.%d\n", compiled.major, compiled.minor, compiled.patch);
        printf("Linking against SDL version %d.%d.%d\n", linked.major, linked.minor, linked.patch);
    }
    
    return 0;
}